# LHCb
set(LHCb_deps
     AIDA
     Boost
     CLHEP
     COOL
     CORAL
     CppUnit
     GSL
     HepMC
     HepPDT
     Python
     QMtest
     Qt
     RELAX
     ROOT
     XercesC
     eigen
     fastjet
     fftw
     #gdb
     doxygen
     gperftools
     graphviz
     libunwind
     #neurobayes_expert
     #oracle
     pyanalysis
     pygraphics
     pytools
     sqlite
     tbb
     #tcmalloc
     #uuid
     vdt
     xqilla
     xrootd
     )
if(GCCXML_native_version)
  list(APPEND LHCb_deps GCCXML)
endif()

add_custom_target(LHCb-pack
                  DEPENDS ${LHCb_deps})
